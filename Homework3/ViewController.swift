//
//  ViewController.swift
//  Homework3
//
//  Created by Valentyn Filippov on 8/22/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        blockOne()
        blockTwo()
        
    }
    
    func blockOne () {
        maxNumber(numberOne: 15, numberTwo: 7)
        squareAndBeyond(number: 5)
        fromZeroAndToZero(number: 7)
        dividersNumberPrint(number: 45)
        isPerfectNumber(number: 8128)
    }
    
    func blockTwo () {
        manhattanCost(initialCost: 24)
        moneyFromMotherCount(scolarship: 700, months: 10, monthlyExpenses: 1000)
        howManyMonthsAlive(scolarship: 700, monthlyExpenses: 1000, personalSavings: 2400)
        reverseTheNumber(number: 97)
    }
    
    func maxNumber (numberOne : Int, numberTwo : Int) {
        if (numberOne == numberTwo){
            print("Numbers are equal")
        } else {
            if (numberOne > numberTwo) {
                print("Bigger number is", numberOne)
            } else {
                print("Bigger number is", numberTwo)
            }
        }
    }
    
    func squareAndBeyond (number : Int) {
        print("Square of the number is", number * number)
        print("Third degree of the number is", number * number * number)
    }
    
    func fromZeroAndToZero (number : Int) {
        for i in 0..<number {
            print(i, ((number-1) - i))
        }
    }
    
    func dividersNumberPrint (number : Int) {
        var numberOfDividers = 0
        for i in 1..<number {
            if ((number % i) == 0) {
                numberOfDividers = numberOfDividers + 1
                print("One of the dividers is", i)
            }
        }
        print("Total number of dividers is", numberOfDividers)
    }
    
    func isPerfectNumber (number : Int) {
        var sumOfDividers = 0
        for i in 1..<number {
            if ((number % i) == 0) {
                sumOfDividers = sumOfDividers + i
            }
        }
        if (sumOfDividers == number) {
            print(number, "is a perfect number")
        } else {
            print(number, "is not a perfect one. In fact, it is bad.")
        }
    }
    
    func manhattanCost (initialCost : Int) {
        let yearDifference = 2018 - 1826
        var finalCost = Double(initialCost)
        for _ in 0..<yearDifference {
            finalCost = Double(finalCost) * 1.06
        }
        print("Final cost is ~ $", Int(finalCost))
    }
    
    func moneyFromMotherCount (scolarship : Int, months : Int, monthlyExpenses : Int) {
        var overAllExpenses = 0
        var afterInflationMonthlyRate = monthlyExpenses
        for _ in 0..<months {
            overAllExpenses = overAllExpenses + afterInflationMonthlyRate
            afterInflationMonthlyRate = Int(Double(afterInflationMonthlyRate) * 1.03)
        }
        print("You need ask your mom to give you", (overAllExpenses - (scolarship * months)), "hryvnas")
    }
    
    func howManyMonthsAlive (scolarship : Int, monthlyExpenses : Int , personalSavings : Int) {
        var monthsAlive = 0
        var totalSavings = personalSavings
        var afterInflationMonthlyRate = monthlyExpenses
        while (totalSavings > 0) {
            totalSavings = totalSavings - afterInflationMonthlyRate + scolarship
            afterInflationMonthlyRate = Int(Double(afterInflationMonthlyRate) * 1.03)
            monthsAlive = monthsAlive + 1
        }
        print("You'll die starving in", monthsAlive, "months, so better give your mother a call")
    }
    
    func reverseTheNumber (number : Int) {
        let digitOne = number % 10
        let digitTwo = number / 10
        print("Reversed number is", ((digitOne * 10) + digitTwo))
    }
    
}

